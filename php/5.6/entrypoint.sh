#! /bin/bash

# escape string
SED_APACHE_LOG_DIR=$(sed -e 's/[]\/$*.^|[]/\\&/g' <<<"$APACHE_LOG_DIR")
SED_VIRTUALHOST=$(sed -e 's/[]\/$*.^|[]/\\&/g' <<<"$VIRTUALHOST")

# change "balise" par leur "valeur"
sed -i -e "s/\[APACHELOGDIR\]/$SED_APACHE_LOG_DIR/g" /etc/apache2/sites-available/*.conf
sed -i -e "s/\[VIRTUALHOST\]/$SED_VIRTUALHOST/g" /etc/apache2/sites-available/*.conf

a2ensite *.conf

apache2-foreground
