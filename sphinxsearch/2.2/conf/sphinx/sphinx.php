<?php

$dirs = [
    '/etc/sphinxsearch/common/',
    '/application/private/application/ressource/sphinx/',
];

$array = [];

foreach ($dirs as $dir) {
    foreach (glob($dir . '*') as $item) {
        $filename = preg_replace('#.*/([^\/\.]+).(php|conf)#si', '$1', $item);
        $extension = preg_replace('#.*/([^\/\.]+).(php|conf)#si', '$2', $item);
        $array[$filename] = [
            'path' => $item,
            'extension' => $extension,
            'filename' => $filename
        ];
    }
}

foreach ($array as $item) {
    if ($item['extension'] == 'php') {
        require $item['path'];
    } else {
        echo file_get_contents($item['path']);
    }

    echo "\n\n\n";
}
